## ansible_date_time

#### for 2024-04-04 10:59:09
```
- 'weekday_number': '4'
- 'iso8601_basic_short': '20240404T125909'
- 'tz': 'CEST'
- 'weeknumber': '14'
- 'hour': '12'
- 'time': '12:59:09'
- 'epoch_int': '1712228349'
- 'tz_offset': '+0200'
- 'month': '04'
- 'epoch': '1712228349'
- 'iso8601_micro': '2024-04-04T10:59:09.939119Z'
- 'weekday': 'Thursday'
- 'iso8601_basic': '20240404T125909939119'
- 'year': '2024'
- 'date': '2024-04-04'
- 'iso8601': '2024-04-04T10:59:09Z'
- 'tz_dst': 'CEST'
- 'day': '04'
- 'minute': '59'
- 'second': '09'
```

### Example

```yml
- name: Print key path if current year in filename
  ansible.builtin.debug:
    msg: "{{ key.stat.path }}"
  when: ansible_date_time.year in key.stat.path
```