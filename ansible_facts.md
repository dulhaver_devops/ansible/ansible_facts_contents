### Ansible facts

```
ansible localhost -m setup | less

localhost | SUCCESS => {
    "ansible_facts": {
        "ansible_all_ipv4_addresses": [
            "10.250.23.81"
        ],
        "ansible_all_ipv6_addresses": [],
        "ansible_apparmor": {
            "status": "disabled"
        },
        "ansible_architecture": "x86_64",
        "ansible_bios_date": "11/12/2020",
        "ansible_bios_vendor": "Phoenix Technologies LTD",
        "ansible_bios_version": "6.00",
        "ansible_board_asset_tag": "NA",
        "ansible_board_name": "440BX Desktop Reference Platform",
        "ansible_board_serial": "NA",
        "ansible_board_vendor": "Intel Corporation",
        "ansible_board_version": "None",
        "ansible_chassis_asset_tag": "No Asset Tag",
        "ansible_chassis_serial": "NA",
        "ansible_chassis_vendor": "No Enclosure",
        "ansible_chassis_version": "N/A",
        "ansible_cmdline": {
            "BOOT_IMAGE": "(hd0,msdos1)/vmlinuz-4.18.0-513.18.1.el8_9.x86_64",
            "crashkernel": "auto",
            "ipv6.disable": "1",
            "net.ifnames": "0",
            "rhgb": true,
            "ro": true,
            "root": "/dev/mapper/vgsys-root",
            "rootflags": "uquota,gquota",
            "transparent_hugepage": "never",
            "vconsole.font": "latarcyrheb-sun16",
            "vconsole.keymap": "de",
            "verbose": true
        },
        "ansible_date_time": {
            "date": "2024-04-05",
            "day": "05",
            "epoch": "1712300945",
            "epoch_int": "1712300945",
            "hour": "09",
            "iso8601": "2024-04-05T07:09:05Z",
            "iso8601_basic": "20240405T090905357192",
            "iso8601_basic_short": "20240405T090905",
            "iso8601_micro": "2024-04-05T07:09:05.357192Z",
            "minute": "09",
            "month": "04",
            "second": "05",
            "time": "09:09:05",
            "tz": "CEST",
            "tz_dst": "CEST",
            "tz_offset": "+0200",
            "weekday": "Friday",
            "weekday_number": "5",
            "weeknumber": "14",
            "year": "2024"
        },
        "ansible_default_ipv4": {
            "address": "10.250.23.81",
            "alias": "eth0",
            "broadcast": "10.250.23.127",
            "gateway": "10.250.23.65",
            "interface": "eth0",
            "macaddress": "00:50:56:82:f5:07",
            "mtu": 1500,
            "netmask": "255.255.255.192",
            "network": "10.250.23.64",
            "prefix": "26",
            "type": "ether"
        },
        "ansible_default_ipv6": {},
        "ansible_device_links": {
            "ids": {
                "dm-0": [
                    "dm-name-vgsys-swap",
                    "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4IbEpza7dfKrTZZG3oXudkF93gT2YywxF"
                ],
                "dm-1": [
                    "dm-name-vgsys-var",
                    "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4he0jZVsdphFik3aZUP2SMZzm5dPKzdhu"
                ],
                "dm-2": [
                    "dm-name-vgsys-root",
                    "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4iyS2smfATComo84sY1kL3yBPTRq3SZcA"
                ],
                "dm-3": [
                    "dm-name-vgsys-home",
                    "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4EhZnrLdpGUop089RPzvtSeKeYiWYPo2f"
                ],
                "dm-4": [
                    "dm-name-vgsys-custom",
                    "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4soeQRyck5G7mVf3tNMf3iywULAUFZHUL"
                ],
                "dm-5": [
                    "dm-name-vgsys-srv",
                    "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk47DUBqzwa4uGEAtcY0ku27RPDnx7KLbP3"
                ],
                "dm-6": [
                    "dm-name-vgsys-mysql",
                    "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4UrGUnnc8QbfVXvzNCzR4ZuYxlHR3ZZMK"
                ],
                "dm-7": [
                    "dm-name-vgsys-data",
                    "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4WyRzxqPPUu2ih326NmDs25WBzcgseKeB"
                ],
                "dm-8": [
                    "dm-name-vgsys-backup",
                    "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4kUd432cldkyEJTQda4T1RW0nzdgtWURZ"
                ],
                "sda2": [
                    "lvm-pv-uuid-R7fqO6-t1bl-nrQT-dYpl-f3o2-rOS7-HyMAcv"
                ],
                "sdb1": [
                    "lvm-pv-uuid-1hiDJo-yU41-dU7z-kyrs-2tNJ-JteL-MbZv7i"
                ],
                "sdc1": [
                    "lvm-pv-uuid-kNqCkv-j0qz-Gqiv-ZGuD-ct1W-d8oo-PyPDqs"
                ],
                "sr0": [
                    "ata-VMware_Virtual_IDE_CDROM_Drive_10000000000000000001"
                ]
            },
            "labels": {
                "dm-1": [
                    "VAR"
                ],
                "dm-2": [
                    "ROOT"
                ],
                "dm-3": [
                    "HOME"
                ],
                "dm-4": [
                    "CUSTOM"
                ],
                "dm-5": [
                    "SRV"
                ],
                "dm-6": [
                    "MYSQL"
                ],
                "dm-7": [
                    "DATA"
                ],
                "dm-8": [
                    "BACKUP"
                ],
                "sda1": [
                    "BOOT"
                ]
            },
            "masters": {
                "sda2": [
                    "dm-6",
                    "dm-7"
                ],
                "sdb1": [
                    "dm-0",
                    "dm-1",
                    "dm-2",
                    "dm-3",
                    "dm-4",
                    "dm-5",
                    "dm-8"
                ],
                "sdc1": [
                    "dm-4"
                ]
            },
            "uuids": {
                "dm-0": [
                    "53618b20-8ce7-4ac5-a8bd-b83204b48d62"
                ],
                "dm-1": [
                    "f0ec8078-2e0a-4bd0-a437-8cf32217295b"
                ],
                "dm-2": [
                    "d60cf373-e383-4fb8-9bac-045b0a659ea4"
                ],
                "dm-3": [
                    "6f0ef854-bd00-4476-b43d-27b5c7b52c53"
                ],
                "dm-4": [
                    "638641a5-f84b-4830-b3cc-f1ccc8adf344"
                ],
                "dm-5": [
                    "3e9b2ea1-76db-4e75-8223-09a3c471c532"
                ],
                "dm-6": [
                    "d535720a-2d6f-443c-938c-854dd0554b50"
                ],
                "dm-7": [
                    "a6306e1b-9c79-4fc8-81f8-875b01b324f1"
                ],
                "dm-8": [
                    "ae066ae8-d64d-45f6-86ac-0cb6388be95c"
                ],
                "sda1": [
                    "19a2bdbe-6979-4191-aaf2-c73b8c9e6c97"
                ]
            }
        },
        "ansible_devices": {
            "dm-0": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [
                        "dm-name-vgsys-swap",
                        "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4IbEpza7dfKrTZZG3oXudkF93gT2YywxF"
                    ],
                    "labels": [],
                    "masters": [],
                    "uuids": [
                        "53618b20-8ce7-4ac5-a8bd-b83204b48d62"
                    ]
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "",
                "sectors": "8388608",
                "sectorsize": "512",
                "size": "4.00 GB",
                "support_discard": "0",
                "vendor": null,
                "virtual": 1
            },
            "dm-1": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [
                        "dm-name-vgsys-var",
                        "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4he0jZVsdphFik3aZUP2SMZzm5dPKzdhu"
                    ],
                    "labels": [
                        "VAR"
                    ],
                    "masters": [],
                    "uuids": [
                        "f0ec8078-2e0a-4bd0-a437-8cf32217295b"
                    ]
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "",
                "sectors": "16777216",
                "sectorsize": "512",
                "size": "8.00 GB",
                "support_discard": "0",
                "vendor": null,
                "virtual": 1
            },
            "dm-2": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [
                        "dm-name-vgsys-root",
                        "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4iyS2smfATComo84sY1kL3yBPTRq3SZcA"
                    ],
                    "labels": [
                        "ROOT"
                    ],
                    "masters": [],
                    "uuids": [
                        "d60cf373-e383-4fb8-9bac-045b0a659ea4"
                    ]
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "",
                "sectors": "14680064",
                "sectorsize": "512",
                "size": "7.00 GB",
                "support_discard": "0",
                "vendor": null,
                "virtual": 1
            },
            "dm-3": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [
                        "dm-name-vgsys-home",
                        "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4EhZnrLdpGUop089RPzvtSeKeYiWYPo2f"
                    ],
                    "labels": [
                        "HOME"
                    ],
                    "masters": [],
                    "uuids": [
                        "6f0ef854-bd00-4476-b43d-27b5c7b52c53"
                    ]
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "",
                "sectors": "20971520",
                "sectorsize": "512",
                "size": "10.00 GB",
                "support_discard": "0",
                "vendor": null,
                "virtual": 1
            },
            "dm-4": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [
                        "dm-name-vgsys-custom",
                        "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4soeQRyck5G7mVf3tNMf3iywULAUFZHUL"
                    ],
                    "labels": [
                        "CUSTOM"
                    ],
                    "masters": [],
                    "uuids": [
                        "638641a5-f84b-4830-b3cc-f1ccc8adf344"
                    ]
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "",
                "sectors": "209756160",
                "sectorsize": "512",
                "size": "100.02 GB",
                "support_discard": "0",
                "vendor": null,
                "virtual": 1
            },
            "dm-5": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [
                        "dm-name-vgsys-srv",
                        "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk47DUBqzwa4uGEAtcY0ku27RPDnx7KLbP3"
                    ],
                    "labels": [
                        "SRV"
                    ],
                    "masters": [],
                    "uuids": [
                        "3e9b2ea1-76db-4e75-8223-09a3c471c532"
                    ]
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "",
                "sectors": "40960",
                "sectorsize": "512",
                "size": "20.00 MB",
                "support_discard": "0",
                "vendor": null,
                "virtual": 1
            },
            "dm-6": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [
                        "dm-name-vgsys-mysql",
                        "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4UrGUnnc8QbfVXvzNCzR4ZuYxlHR3ZZMK"
                    ],
                    "labels": [
                        "MYSQL"
                    ],
                    "masters": [],
                    "uuids": [
                        "d535720a-2d6f-443c-938c-854dd0554b50"
                    ]
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "",
                "sectors": "20971520",
                "sectorsize": "512",
                "size": "10.00 GB",
                "support_discard": "1048576",
                "vendor": null,
                "virtual": 1
            },
            "dm-7": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [
                        "dm-name-vgsys-data",
                        "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4WyRzxqPPUu2ih326NmDs25WBzcgseKeB"
                    ],
                    "labels": [
                        "DATA"
                    ],
                    "masters": [],
                    "uuids": [
                        "a6306e1b-9c79-4fc8-81f8-875b01b324f1"
                    ]
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "",
                "sectors": "20971520",
                "sectorsize": "512",
                "size": "10.00 GB",
                "support_discard": "1048576",
                "vendor": null,
                "virtual": 1
            },
            "dm-8": {
                "holders": [],
                "host": "",
                "links": {
                    "ids": [
                        "dm-name-vgsys-backup",
                        "dm-uuid-LVM-KQXKWA3ak7Gz3ant3PwsDEs1YBjVIRk4kUd432cldkyEJTQda4T1RW0nzdgtWURZ"
                    ],
                    "labels": [
                        "BACKUP"
                    ],
                    "masters": [],
                    "uuids": [
                        "ae066ae8-d64d-45f6-86ac-0cb6388be95c"
                    ]
                },
                "model": null,
                "partitions": {},
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "",
                "sectors": "10485760",
                "sectorsize": "512",
                "size": "5.00 GB",
                "support_discard": "0",
                "vendor": null,
                "virtual": 1
            },
            "sda": {
                "holders": [],
                "host": "Serial Attached SCSI controller: VMware PVSCSI SCSI Controller (rev 02)",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": "Virtual disk",
                "partitions": {
                    "sda1": {
                        "holders": [],
                        "links": {
                            "ids": [],
                            "labels": [
                                "BOOT"
                            ],
                            "masters": [],
                            "uuids": [
                                "19a2bdbe-6979-4191-aaf2-c73b8c9e6c97"
                            ]
                        },
                        "sectors": "2097152",
                        "sectorsize": 512,
                        "size": "1.00 GB",
                        "start": "2048",
                        "uuid": "19a2bdbe-6979-4191-aaf2-c73b8c9e6c97"
                    },
                    "sda2": {
                        "holders": [
                            "vgsys-mysql",
                            "vgsys-data"
                        ],
                        "links": {
                            "ids": [
                                "lvm-pv-uuid-R7fqO6-t1bl-nrQT-dYpl-f3o2-rOS7-HyMAcv"
                            ],
                            "labels": [],
                            "masters": [
                                "dm-6",
                                "dm-7"
                            ],
                            "uuids": []
                        },
                        "sectors": "50329600",
                        "sectorsize": 512,
                        "size": "24.00 GB",
                        "start": "2099200",
                        "uuid": null
                    }
                },
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "mq-deadline",
                "sectors": "52428800",
                "sectorsize": "512",
                "size": "25.00 GB",
                "support_discard": "1048576",
                "vendor": "VMware",
                "virtual": 1
            },
            "sdb": {
                "holders": [],
                "host": "Serial Attached SCSI controller: VMware PVSCSI SCSI Controller (rev 02)",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": "Virtual disk",
                "partitions": {
                    "sdb1": {
                        "holders": [
                            "vgsys-var",
                            "vgsys-backup",
                            "vgsys-custom",
                            "vgsys-root",
                            "vgsys-swap",
                            "vgsys-srv",
                            "vgsys-home"
                        ],
                        "links": {
                            "ids": [
                                "lvm-pv-uuid-1hiDJo-yU41-dU7z-kyrs-2tNJ-JteL-MbZv7i"
                            ],
                            "labels": [],
                            "masters": [
                                "dm-0",
                                "dm-1",
                                "dm-2",
                                "dm-3",
                                "dm-4",
                                "dm-5",
                                "dm-8"
                            ],
                            "uuids": []
                        },
                        "sectors": "73398272",
                        "sectorsize": 512,
                        "size": "35.00 GB",
                        "start": "2048",
                        "uuid": null
                    }
                },
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "mq-deadline",
                "sectors": "73400320",
                "sectorsize": "512",
                "size": "35.00 GB",
                "support_discard": "0",
                "vendor": "VMware",
                "virtual": 1
            },
            "sdc": {
                "holders": [],
                "host": "Serial Attached SCSI controller: VMware PVSCSI SCSI Controller (rev 02)",
                "links": {
                    "ids": [],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": "Virtual disk",
                "partitions": {
                    "sdc1": {
                        "holders": [
                            "vgsys-custom"
                        ],
                        "links": {
                            "ids": [
                                "lvm-pv-uuid-kNqCkv-j0qz-Gqiv-ZGuD-ct1W-d8oo-PyPDqs"
                            ],
                            "labels": [],
                            "masters": [
                                "dm-4"
                            ],
                            "uuids": []
                        },
                        "sectors": "209713152",
                        "sectorsize": 512,
                        "size": "100.00 GB",
                        "start": "2048",
                        "uuid": null
                    }
                },
                "removable": "0",
                "rotational": "0",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "mq-deadline",
                "sectors": "209715200",
                "sectorsize": "512",
                "size": "100.00 GB",
                "support_discard": "0",
                "vendor": "VMware",
                "virtual": 1
            },
            "sr0": {
                "holders": [],
                "host": "IDE interface: Intel Corporation 82371AB/EB/MB PIIX4 IDE (rev 01)",
                "links": {
                    "ids": [
                        "ata-VMware_Virtual_IDE_CDROM_Drive_10000000000000000001"
                    ],
                    "labels": [],
                    "masters": [],
                    "uuids": []
                },
                "model": "VMware IDE CDR10",
                "partitions": {},
                "removable": "1",
                "rotational": "1",
                "sas_address": null,
                "sas_device_handle": null,
                "scheduler_mode": "mq-deadline",
                "sectors": "2097151",
                "sectorsize": "512",
                "size": "1024.00 MB",
                "support_discard": "0",
                "vendor": "NECVMWar",
                "virtual": 1
            }
        },
        "ansible_distribution": "RedHat",
        "ansible_distribution_file_parsed": true,
        "ansible_distribution_file_path": "/etc/redhat-release",
        "ansible_distribution_file_search_string": "Red Hat",
        "ansible_distribution_file_variety": "RedHat",
        "ansible_distribution_major_version": "8",
        "ansible_distribution_release": "Ootpa",
        "ansible_distribution_version": "8.9",
        "ansible_dns": {
            "nameservers": [
                "10.250.2.226",
                "10.250.2.227"
            ],
            "search": [
                "rz-dvz.cn-mv.de",
                "intranet.cn-mv.de",
                "portal.cn-mv.de"
            ]
        },
        "ansible_domain": "dbmon.rz-dvz.cn-mv.de",
        "ansible_effective_group_id": 100,
        "ansible_effective_user_id": 3381,
        "ansible_env": {
            "BASH_FUNC_generate_md5_hash%%": "() {  echo \"md5$(echo -n $1 | md5sum | awk '{print $1}')\"\n}",
            "BASH_FUNC_which%%": "() {  ( alias;\n eval ${which_declare} ) | /usr/bin/which --tty-only --read-alias --read-functions --show-tilde --show-dot $@\n}",
            "DBUS_SESSION_BUS_ADDRESS": "unix:path=/run/user/3381/bus",
            "HISTCONTROL": "ignoredups",
            "HISTFILE": "/home/gwagner/.history/gwagner",
            "HISTSIZE": "1000",
            "HISTTIMEFORMAT": "%h/%d - %H:%M:%S ",
            "HOME": "/home/gwagner",
            "HOSTNAME": "dvzsn-rd1985",
            "LANG": "en_US.UTF-8",
            "LC_MESSAGES": "en_US.UTF-8",
            "LESSOPEN": "||/usr/bin/lesspipe.sh %s",
            "LOGNAME": "gwagner",
            "LS_COLORS": "rs=0:di=38;5;33:ln=38;5;51:mh=00:pi=40;38;5;11:so=38;5;13:do=38;5;5:bd=48;5;232;38;5;11:cd=48;5;232;38;5;3:or=48;5;232;38;5;9:mi=01;05;37;41:su=48;5;196;38;5;15:sg=48;5;11;38;5;16:ca=48;5;196;38;5;226:tw=48;5;10;38;5;16:ow=48;5;10;38;5;21:st=48;5;21;38;5;15:ex=38;5;40:*.tar=38;5;9:*.tgz=38;5;9:*.arc=38;5;9:*.arj=38;5;9:*.taz=38;5;9:*.lha=38;5;9:*.lz4=38;5;9:*.lzh=38;5;9:*.lzma=38;5;9:*.tlz=38;5;9:*.txz=38;5;9:*.tzo=38;5;9:*.t7z=38;5;9:*.zip=38;5;9:*.z=38;5;9:*.dz=38;5;9:*.gz=38;5;9:*.lrz=38;5;9:*.lz=38;5;9:*.lzo=38;5;9:*.xz=38;5;9:*.zst=38;5;9:*.tzst=38;5;9:*.bz2=38;5;9:*.bz=38;5;9:*.tbz=38;5;9:*.tbz2=38;5;9:*.tz=38;5;9:*.deb=38;5;9:*.rpm=38;5;9:*.jar=38;5;9:*.war=38;5;9:*.ear=38;5;9:*.sar=38;5;9:*.rar=38;5;9:*.alz=38;5;9:*.ace=38;5;9:*.zoo=38;5;9:*.cpio=38;5;9:*.7z=38;5;9:*.rz=38;5;9:*.cab=38;5;9:*.wim=38;5;9:*.swm=38;5;9:*.dwm=38;5;9:*.esd=38;5;9:*.jpg=38;5;13:*.jpeg=38;5;13:*.mjpg=38;5;13:*.mjpeg=38;5;13:*.gif=38;5;13:*.bmp=38;5;13:*.pbm=38;5;13:*.pgm=38;5;13:*.ppm=38;5;13:*.tga=38;5;13:*.xbm=38;5;13:*.xpm=38;5;13:*.tif=38;5;13:*.tiff=38;5;13:*.png=38;5;13:*.svg=38;5;13:*.svgz=38;5;13:*.mng=38;5;13:*.pcx=38;5;13:*.mov=38;5;13:*.mpg=38;5;13:*.mpeg=38;5;13:*.m2v=38;5;13:*.mkv=38;5;13:*.webm=38;5;13:*.ogm=38;5;13:*.mp4=38;5;13:*.m4v=38;5;13:*.mp4v=38;5;13:*.vob=38;5;13:*.qt=38;5;13:*.nuv=38;5;13:*.wmv=38;5;13:*.asf=38;5;13:*.rm=38;5;13:*.rmvb=38;5;13:*.flc=38;5;13:*.avi=38;5;13:*.fli=38;5;13:*.flv=38;5;13:*.gl=38;5;13:*.dl=38;5;13:*.xcf=38;5;13:*.xwd=38;5;13:*.yuv=38;5;13:*.cgm=38;5;13:*.emf=38;5;13:*.ogv=38;5;13:*.ogx=38;5;13:*.aac=38;5;45:*.au=38;5;45:*.flac=38;5;45:*.m4a=38;5;45:*.mid=38;5;45:*.midi=38;5;45:*.mka=38;5;45:*.mp3=38;5;45:*.mpc=38;5;45:*.ogg=38;5;45:*.ra=38;5;45:*.wav=38;5;45:*.oga=38;5;45:*.opus=38;5;45:*.spx=38;5;45:*.xspf=38;5;45:",
            "MAIL": "/var/spool/mail/gwagner",
            "OLDPWD": "/home/gwagner",
            "PATH": "/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/gwagner/.local/bin:/home/gwagner/bin",
            "PWD": "/home/gwagner/repos/ad_hoc_playbooks",
            "SELINUX_LEVEL_REQUESTED": "",
            "SELINUX_ROLE_REQUESTED": "",
            "SELINUX_USE_CURRENT_RANGE": "",
            "SHELL": "/bin/bash",
            "SHLVL": "3",
            "SSH_AUTH_SOCK": "/tmp/ssh-Ghq37CLpLB/agent.861112",
            "SSH_CLIENT": "10.250.249.56 61310 22",
            "SSH_CONNECTION": "10.250.249.56 61310 10.250.23.81 22",
            "SSH_TTY": "/dev/pts/2",
            "S_COLORS": "auto",
            "TERM": "xterm-256color",
            "USER": "gwagner",
            "XDG_RUNTIME_DIR": "/run/user/3381",
            "XDG_SESSION_ID": "79912",
            "_": "/usr/bin/python3.11",
            "which_declare": "declare -f"
        },
        "ansible_eth0": {
            "active": true,
            "device": "eth0",
            "ipv4": {
                "address": "10.250.23.81",
                "broadcast": "10.250.23.127",
                "netmask": "255.255.255.192",
                "network": "10.250.23.64",
                "prefix": "26"
            },
            "macaddress": "00:50:56:82:f5:07",
            "module": "vmxnet3",
            "mtu": 1500,
            "pciid": "0000:0b:00.0",
            "promisc": false,
            "speed": 10000,
            "type": "ether"
        },
        "ansible_fibre_channel_wwn": [],
        "ansible_fips": false,
        "ansible_form_factor": "Other",
        "ansible_fqdn": "dvzsn-rd1985.dbmon.rz-dvz.cn-mv.de",
        "ansible_hostname": "dvzsn-rd1985",
        "ansible_hostnqn": "",
        "ansible_interfaces": [
            "lo",
            "eth0"
        ],
        "ansible_is_chroot": false,
        "ansible_iscsi_iqn": "",
        "ansible_kernel": "4.18.0-513.18.1.el8_9.x86_64",
        "ansible_kernel_version": "#1 SMP Thu Feb 1 03:51:05 EST 2024",
        "ansible_lo": {
            "active": true,
            "device": "lo",
            "ipv4": {
                "address": "127.0.0.1",
                "broadcast": "",
                "netmask": "255.0.0.0",
                "network": "127.0.0.0",
                "prefix": "8"
            },
            "mtu": 65536,
            "promisc": false,
            "type": "loopback"
        },
        "ansible_loadavg": {
            "15m": 0.0,
            "1m": 0.0,
            "5m": 0.01
        },
        "ansible_local": {},
        "ansible_locally_reachable_ips": {
            "ipv4": [
                "10.250.23.81",
                "127.0.0.0/8",
                "127.0.0.1"
            ],
            "ipv6": []
        },
        "ansible_lsb": {},
        "ansible_lvm": "N/A",
        "ansible_machine": "x86_64",
        "ansible_machine_id": "11399171612e449da9ac1ad41bb91f51",
        "ansible_memfree_mb": 535,
        "ansible_memory_mb": {
            "nocache": {
                "free": 5537,
                "used": 2116
            },
            "real": {
                "free": 535,
                "total": 7653,
                "used": 7118
            },
            "swap": {
                "cached": 0,
                "free": 4069,
                "total": 4095,
                "used": 26
            }
        },
        "ansible_memtotal_mb": 7653,
        "ansible_mounts": [
            {
                "block_available": 855381,
                "block_size": 4096,
                "block_total": 1832448,
                "block_used": 977067,
                "device": "/dev/mapper/vgsys-root",
                "fstype": "xfs",
                "inode_available": 3579405,
                "inode_total": 3670016,
                "inode_used": 90611,
                "mount": "/",
                "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,usrquota,grpquota",
                "size_available": 3503640576,
                "size_total": 7505707008,
                "uuid": "d60cf373-e383-4fb8-9bac-045b0a659ea4"
            },
            {
                "block_available": 2517197,
                "block_size": 4096,
                "block_total": 2618880,
                "block_used": 101683,
                "device": "/dev/mapper/vgsys-data",
                "fstype": "xfs",
                "inode_available": 5242514,
                "inode_total": 5242880,
                "inode_used": 366,
                "mount": "/opt/db/data",
                "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                "size_available": 10310438912,
                "size_total": 10726932480,
                "uuid": "a6306e1b-9c79-4fc8-81f8-875b01b324f1"
            },
            {
                "block_available": 2152782,
                "block_size": 4096,
                "block_total": 2618880,
                "block_used": 466098,
                "device": "/dev/mapper/vgsys-mysql",
                "fstype": "xfs",
                "inode_available": 5204756,
                "inode_total": 5242880,
                "inode_used": 38124,
                "mount": "/opt/db/mysql",
                "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                "size_available": 8817795072,
                "size_total": 10726932480,
                "uuid": "d535720a-2d6f-443c-938c-854dd0554b50"
            },
            {
                "block_available": 213200,
                "block_size": 4096,
                "block_total": 259584,
                "block_used": 46384,
                "device": "/dev/sda1",
                "fstype": "xfs",
                "inode_available": 523968,
                "inode_total": 524288,
                "inode_used": 320,
                "mount": "/boot",
                "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                "size_available": 873267200,
                "size_total": 1063256064,
                "uuid": "19a2bdbe-6979-4191-aaf2-c73b8c9e6c97"
            },
            {
                "block_available": 57770,
                "block_size": 4096,
                "block_total": 2618880,
                "block_used": 2561110,
                "device": "/dev/mapper/vgsys-home",
                "fstype": "xfs",
                "inode_available": 463788,
                "inode_total": 556880,
                "inode_used": 93092,
                "mount": "/home",
                "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                "size_available": 236625920,
                "size_total": 10726932480,
                "uuid": "6f0ef854-bd00-4476-b43d-27b5c7b52c53"
            },
            {
                "block_available": 1298588,
                "block_size": 4096,
                "block_total": 1308160,
                "block_used": 9572,
                "device": "/dev/mapper/vgsys-backup",
                "fstype": "xfs",
                "inode_available": 2621431,
                "inode_total": 2621440,
                "inode_used": 9,
                "mount": "/opt/db/backup",
                "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                "size_available": 5319016448,
                "size_total": 5358223360,
                "uuid": "ae066ae8-d64d-45f6-86ac-0cb6388be95c"
            },
            {
                "block_available": 3983,
                "block_size": 4096,
                "block_total": 4265,
                "block_used": 282,
                "device": "/dev/mapper/vgsys-srv",
                "fstype": "xfs",
                "inode_available": 10237,
                "inode_total": 10240,
                "inode_used": 3,
                "mount": "/srv",
                "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                "size_available": 16314368,
                "size_total": 17469440,
                "uuid": "3e9b2ea1-76db-4e75-8223-09a3c471c532"
            },
            {
                "block_available": 1733314,
                "block_size": 4096,
                "block_total": 2094592,
                "block_used": 361278,
                "device": "/dev/mapper/vgsys-var",
                "fstype": "xfs",
                "inode_available": 4188991,
                "inode_total": 4194304,
                "inode_used": 5313,
                "mount": "/var",
                "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                "size_available": 7099654144,
                "size_total": 8579448832,
                "uuid": "f0ec8078-2e0a-4bd0-a437-8cf32217295b"
            },
            {
                "block_available": 4127978,
                "block_size": 4096,
                "block_total": 26218665,
                "block_used": 22090687,
                "device": "/dev/mapper/vgsys-custom",
                "fstype": "xfs",
                "inode_available": 33024223,
                "inode_total": 33025424,
                "inode_used": 1201,
                "mount": "/opt/custom",
                "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                "size_available": 16908197888,
                "size_total": 107391651840,
                "uuid": "638641a5-f84b-4830-b3cc-f1ccc8adf344"
            }
        ],
        "ansible_nodename": "dvzsn-rd1985",
        "ansible_os_family": "RedHat",
        "ansible_pkg_mgr": "dnf",
        "ansible_proc_cmdline": {
            "BOOT_IMAGE": "(hd0,msdos1)/vmlinuz-4.18.0-513.18.1.el8_9.x86_64",
            "crashkernel": "auto",
            "ipv6.disable": "1",
            "net.ifnames": "0",
            "rhgb": true,
            "ro": true,
            "root": "/dev/mapper/vgsys-root",
            "rootflags": "uquota,gquota",
            "transparent_hugepage": "never",
            "vconsole.font": "latarcyrheb-sun16",
            "vconsole.keymap": "de",
            "verbose": true
        },
        "ansible_processor": [
            "0",
            "GenuineIntel",
            "Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz",
            "1",
            "GenuineIntel",
            "Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz"
        ],
        "ansible_processor_cores": 1,
        "ansible_processor_count": 2,
        "ansible_processor_nproc": 2,
        "ansible_processor_threads_per_core": 1,
        "ansible_processor_vcpus": 2,
        "ansible_product_name": "VMware Virtual Platform",
        "ansible_product_serial": "NA",
        "ansible_product_uuid": "NA",
        "ansible_product_version": "None",
        "ansible_python": {
            "executable": "/usr/bin/python3.11",
            "has_sslcontext": true,
            "type": "cpython",
            "version": {
                "major": 3,
                "micro": 5,
                "minor": 11,
                "releaselevel": "final",
                "serial": 0
            },
            "version_info": [
                3,
                11,
                5,
                "final",
                0
            ]
        },
        "ansible_python_version": "3.11.5",
        "ansible_real_group_id": 100,
        "ansible_real_user_id": 3381,
        "ansible_selinux": {
            "config_mode": "permissive",
            "mode": "permissive",
            "policyvers": 33,
            "status": "enabled",
            "type": "targeted"
        },
        "ansible_selinux_python_present": true,
        "ansible_service_mgr": "systemd",
        "ansible_ssh_host_key_dsa_public": "AAAAB3NzaC1kc3MAAACBALZUZmW3k8PlXbHWF3Wt1R10JSfr/c3SJXfEetuVpgqRURGPcHSnEC5H4Folg6AOWytCbYJggjdG22yOezILkiDF0KXx2ADXymKtoeNec08CTrBlw2AXpSsPQLyqaHVvASQgpTXZ5fv/wtYQdvkce5TkBZEYMSXea0PIk0w8TKAZAAAAFQD5o0l2YCddstTGxn/ENrlFrbcmpQAAAIEAgS1npVXkxSreMvpsGlLuOdIcpHjRO/33/a3erGCsBLpEh2O2pcGCJyy0Q3MG1TkNotrNsktJugVTgOOt6dlruVlpAbY+2VTyoolPf4Q8AQcxfARNO/gjW3XZGOYZNZWz6swrhplsQGfsSHDFFMw5MqN5gAasdavMzn/jV7oW6nIAAACAJLawci0zbrRAfjuxCRMJ/ooT5TeABzScK8K6DYWEuzCBjIOng0sSn7iXGez62lPm94QeCAXJ8TZSGSzDXLTULC/dDzdvM4zSwtqK70N/EjxsZ9nTZXW07kpJV1wzgcxiOBAdQAOtB87r2H1/8etNK1j4BqCuCGYEaFKiCYjbwws=",
        "ansible_ssh_host_key_dsa_public_keytype": "ssh-dss",
        "ansible_ssh_host_key_ecdsa_public": "AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHm7yDpiGxSjmsxhbjc/0FJNxLpux/6KS1Mp1VZmeJwwr8v1yjz46C5z+zNOq4CpHvbaxCprgJ6wHEh4tS1YFng=",
        "ansible_ssh_host_key_ecdsa_public_keytype": "ecdsa-sha2-nistp256",
        "ansible_ssh_host_key_ed25519_public": "AAAAC3NzaC1lZDI1NTE5AAAAIJ0/KpH5lGQETVmZE4S3+EuFosm0gZX1IVHxFjDs79rG",
        "ansible_ssh_host_key_ed25519_public_keytype": "ssh-ed25519",
        "ansible_ssh_host_key_rsa_public": "AAAAB3NzaC1yc2EAAAADAQABAAABAQCo5O5So5VtJeKYrrhMvsd9I5BO8BW8Np0k3xDVeGb+yM6RSnWOjrg+RoRTcDJYnaDVvKF5cEwKLQuTegwen8tdN9CVarvgZlvSm/IZL1a/5BOokAtWqfgORBxC6baWK23gfmbG7PcCS2CZchW4Neiac0Nk59N1t2Wtu23MK1cZj3flZ1SMs2CfyUuDRYNpsBft0kVegcJt6DaycjPZrk7lKcxvIbtbQBoSHNs49zdVxA2BB2AJs+YiY4w/ATHPXj6LDTGHcimpgzjYugAZ26q7HIZRfZG+u86ZcVJZYIgJZvvW3nYGJOxCGbTqdWJ/k/Y9jd1vK8yj9aJvXZQnJEd7",
        "ansible_ssh_host_key_rsa_public_keytype": "ssh-rsa",
        "ansible_swapfree_mb": 4069,
        "ansible_swaptotal_mb": 4095,
        "ansible_system": "Linux",
        "ansible_system_capabilities": [
            ""
        ],
        "ansible_system_capabilities_enforced": "True",
        "ansible_system_vendor": "VMware, Inc.",
        "ansible_uptime_seconds": 2775289,
        "ansible_user_dir": "/home/gwagner",
        "ansible_user_gecos": "Wagner,Gunnar,INA",
        "ansible_user_gid": 100,
        "ansible_user_id": "gwagner",
        "ansible_user_shell": "/bin/bash",
        "ansible_user_uid": 3381,
        "ansible_userspace_architecture": "x86_64",
        "ansible_userspace_bits": "64",
        "ansible_virtualization_role": "guest",
        "ansible_virtualization_tech_guest": [
            "VMware"
        ],
        "ansible_virtualization_tech_host": [],
        "ansible_virtualization_type": "VMware",
        "gather_subset": [
            "all"
        ],
        "module_setup": true
    },
    "changed": false
}

```